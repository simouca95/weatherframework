# ``WeatherFramwork``

An iOS framework used to get weather information from a specific location through Latitude and Longitude parameters. 

## Overview

Using https://openweathermap.org/api/one-call-api: to access the live weather details.


## Installation
 Use this commands to archive the framework

### Archive for iOS

xcodebuild archive \
-scheme WeatherFramwork \
-configuration Release \
-destination 'generic/platform=iOS' \
-archivePath './build/WeatherFramwork.framework-iphoneos.xcarchive' \
SKIP_INSTALL=NO \
BUILD_LIBRARIES_FOR_DISTRIBUTION=YES


### Archive for iOS Simulator

xcodebuild archive \
-scheme WeatherFramwork \
-configuration Release \
-destination 'generic/platform=iOS Simulator' \
-archivePath './build/WeatherFramwork.framework-iphonesimulator.xcarchive' \
SKIP_INSTALL=NO \
BUILD_LIBRARIES_FOR_DISTRIBUTION=YES


### Create Framework

xcodebuild -create-xcframework \
-framework './build/WeatherFramwork.framework-iphonesimulator.xcarchive/Products/Library/Frameworks/WeatherFramwork.framework' \
-framework './build/WeatherFramwork.framework-iphoneos.xcarchive/Products/Library/Frameworks/WeatherFramwork.framework' \
-output './build/WeatherFramwork.xcframework'


- Add manually the created XCFramework to (in our case) the Swift Package project
and push the new changes into you repo with a new created tag.

