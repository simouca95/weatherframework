//
//  MeteoResponse.swift
//  WeatherFramwork
//
//  Created by sami hazel on 24/09/2022.
//

import Foundation

// MARK: - MeteoResponse
public struct MeteoResponse: Codable {
    public let lat, lon: Double?
    public let timezone: String?
    public let timezoneOffset: Int?
    public let current: Current?
    public let minutely: [Minutely]?
    public let hourly: [Current]?
    public let daily: [Daily]?
    public let alerts: [Alert]?

    enum CodingKeys: String, CodingKey {
        case lat, lon, timezone
        case timezoneOffset = "timezone_offset"
        case current, minutely, hourly, daily, alerts
    }
    
    static let mockedResponseJson = """
{
    "lat": 43.2841,
    "lon": 5.3754,
    "timezone": "Europe/Paris",
    "timezone_offset": 7200,
    "current": {
        "dt": 1664029743,
        "sunrise": 1663997260,
        "sunset": 1664040811,
        "temp": 293.97,
        "feels_like": 292.92,
        "pressure": 1017,
        "humidity": 31,
        "dew_point": 276.24,
        "uvi": 2.5,
        "clouds": 95,
        "visibility": 10000,
        "wind_speed": 2.91,
        "wind_deg": 103,
        "wind_gust": 4.31,
        "weather": [
            {
                "id": 804,
                "main": "Clouds",
                "description": "overcast clouds",
                "icon": "04d"
            }
        ]
    }
}
"""
    
}
