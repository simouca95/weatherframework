//
//  Current.swift
//  WeatherFramwork
//
//  Created by sami hazel on 24/09/2022.
//

import Foundation

// MARK: - Current
public struct Current: Codable {
    public let dt, sunrise, sunset: Int?
    public let temp, feelsLike: Double?
    public let pressure, humidity: Int?
    public let dewPoint, uvi: Double?
    public let clouds, visibility: Int?
    public let windSpeed: Double?
    public let windDeg: Int?
    public let weather: [Weather]?
    public let rain: Rain?
    public let windGust: Double?
    public let pop: Int?

    enum CodingKeys: String, CodingKey {
        case dt, sunrise, sunset, temp
        case feelsLike = "feels_like"
        case pressure, humidity
        case dewPoint = "dew_point"
        case uvi, clouds, visibility
        case windSpeed = "wind_speed"
        case windDeg = "wind_deg"
        case weather, rain
        case windGust = "wind_gust"
        case pop
    }
}
