//
//  Alert.swift
//  WeatherFramwork
//
//  Created by sami hazel on 24/09/2022.
//
// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let meteoResponse = try? newJSONDecoder().decode(MeteoResponse.self, from: jsonData)

import Foundation

// MARK: - Alert
public struct Alert: Codable {
    let senderName, event: String?
    let start, end: Int?
    let alertDescription: String?
    let tags: [String]?

    enum CodingKeys: String, CodingKey {
        case senderName = "sender_name"
        case event, start, end
        case alertDescription = "description"
        case tags
    }
}

// MARK: - Rain
public struct Rain: Codable {
    let the1H: Double?

    enum CodingKeys: String, CodingKey {
        case the1H = "1h"
    }
}

// MARK: - Daily
public struct Daily: Codable {
    public let dt, sunrise, sunset, moonrise: Int?
    public let moonset: Int?
    public let moonPhase: Double?
    public let temp: Temp?
    public let feelsLike: FeelsLike?
    public let pressure, humidity: Int?
    public let dewPoint, windSpeed: Double?
    public let windDeg: Int?
    public let weather: [Weather]?
    public let clouds: Int?
    public let pop, rain, uvi: Double?

    enum CodingKeys: String, CodingKey {
        case dt, sunrise, sunset, moonrise, moonset
        case moonPhase = "moon_phase"
        case temp
        case feelsLike = "feels_like"
        case pressure, humidity
        case dewPoint = "dew_point"
        case windSpeed = "wind_speed"
        case windDeg = "wind_deg"
        case weather, clouds, pop, rain, uvi
    }
}

// MARK: - FeelsLike
public struct FeelsLike: Codable {
    public let day, night, eve, morn: Double?
}

// MARK: - Temp
public struct Temp: Codable {
    public let day, min, max, night: Double?
    public let eve, morn: Double?
}

// MARK: - Minutely
public struct Minutely: Codable {
    public let dt: Int?
    public let precipitation: Double?
}
