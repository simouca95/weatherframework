//
//  APIClient.swift
//  WeatherFramwork
//
//  Created by sami hazel on 24/09/2022.
//

import Foundation

class APIClient {
    typealias completeClosure<T> = Result<T,Environment.APIError>
    private let session: URLSession
    init(session: URLSession) {
        self.session = session
    }
    func getCurrentWeather(with apiKey: String,
                           lat: String,
                           lon: String,
                           completion: @escaping (completeClosure<MeteoResponse>) -> Void) {

        var urlString = Environment.API.baseURL.replacingOccurrences(of: "{lat}", with: lat)
        urlString = urlString.replacingOccurrences(of: "{lon}", with: lon)
        urlString = urlString.replacingOccurrences(of: "{apiKey}", with: apiKey)
        
        guard let url = URL(string: urlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!) else {
            completion(.failure(.invalidURL))
            return
        }
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        let task = session.dataTask(with: url) { (data, response, error) in

            if error != nil {
                completion(.failure(.networkFailer))
            } else if
                let data = data,
                let response = response as? HTTPURLResponse,
                response.statusCode == 200 {
                if let decodedResponse = try? JSONDecoder().decode(MeteoResponse.self, from: data) {
                    completion(.success(decodedResponse))
                } else {
                    completion(.failure(.decodingFailer))
                }
            } else {
                completion(.failure(.networkFailer))
            }
            return
        }
        task.resume()
    }
}
