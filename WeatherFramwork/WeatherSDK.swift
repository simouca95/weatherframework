//
//  WeatherSDK.swift
//  WeatherFramwork
//
//  Created by sami hazel on 24/09/2022.
//

import Foundation

public class WeatherSDK {

    private let apiKey: String

    public init(apiKey: String) {
        self.apiKey = apiKey
    }

    public func getCurrentWeather(from lat: String, lon: String, completion: @escaping (Result<MeteoResponse,Environment.APIError>) -> Void) {
        // 1
        let urlSession = URLSession(configuration: .default)
        
        let weatherAPIClient = APIClient(session: urlSession)
        
        weatherAPIClient.getCurrentWeather(with: self.apiKey, lat: lat, lon: lon, completion: completion)
    }
}
