//
//  WeatherFramworkTests.swift
//  WeatherFramworkTests
//
//  Created by sami hazel on 24/09/2022.
//

import XCTest
@testable import WeatherFramwork

class WeatherFramworkTests: XCTestCase {

    let testLat = "43.28408"
    let testLon = "5.3754"
    let testActiveApiKey = "c4c6b41514600215d4679681e403dac7"
    let testWrongApiKey = "112233445566aabbccdd"

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        // Any test you write for XCTest can be annotated as throws and async.
        // Mark your test throws to produce an unexpected failure when your test encounters an uncaught error.
        // Mark your test async to allow awaiting for asynchronous code to complete. Check the results with assertions afterwards.
    }
    
    func testAPICall() throws {
        var urlString = Environment.API.baseURL.replacingOccurrences(of: "{lat}", with: testLat)
        urlString = urlString.replacingOccurrences(of: "{lon}", with: testLon)
        urlString = urlString.replacingOccurrences(of: "{apiKey}", with: testActiveApiKey)
        if let urlComponents = URLComponents(string: urlString) {
            guard let url = urlComponents.url else {
                return
            }
            let dataTask = URLSession.shared.dataTask(with: url) { data, response, error in

                if let error = error {
                    XCTFail("Test failed because: \(error.localizedDescription)")
                } else if let response = response as? HTTPURLResponse {
                    XCTAssertEqual(response.statusCode, 200)
                } else {
                    XCTFail("Expected to be a success withh 200 but failed to cast response to HTTPURLResponse")
                }
            }
            // 7
            dataTask.resume()
        }
    }
    
    func testWrongApiKeyAPICall() throws {
        var urlString = Environment.API.baseURL.replacingOccurrences(of: "{lat}", with: testLat)
        urlString = urlString.replacingOccurrences(of: "{lon}", with: testLon)
        urlString = urlString.replacingOccurrences(of: "{apiKey}", with: testWrongApiKey)
        if let urlComponents = URLComponents(string: urlString) {
            guard let url = urlComponents.url else {
                return
            }
            let dataTask = URLSession.shared.dataTask(with: url) { data, response, error in

                if let error = error {
                    XCTFail("Test failed because: \(error.localizedDescription)")
                } else if let response = response as? HTTPURLResponse {
                    XCTAssertEqual(response.statusCode, 200)
                } else {
                    XCTFail("Expected to be a success withh 200 but failed to cast response to HTTPURLResponse")
                }
            }
            // 7
            dataTask.resume()
        }
    }
    


    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
