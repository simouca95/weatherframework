//
//  WeatherAPIClientTest.swift
//  WeatherFramworkTests
//
//  Created by sami hazel on 24/09/2022.
//

import XCTest

class WeatherAPIClientTest: XCTestCase {

    var weatherAPI: APIClient!
    var expectation: XCTestExpectation!
    let testLat = "43.28408"
    let testLon = "5.3754"
    let testActiveApiKey = "c4c6b41514600215d4679681e403dac7"

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        let configuration = URLSessionConfiguration.default
        configuration.protocolClasses = [MockURLProtocol.self]
        let urlSession = URLSession(configuration: configuration)
        
        weatherAPI = APIClient(session: urlSession)
        expectation = expectation(description: "Expectation")
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testAPIRequestGeneration() throws {
        
        var urlString = Environment.API.baseURL.replacingOccurrences(of: "{lat}", with: testLat)
        urlString = urlString.replacingOccurrences(of: "{lon}", with: testLon)
        urlString = urlString.replacingOccurrences(of: "{apiKey}", with: testActiveApiKey)
        let urlComponents = URLComponents(string: urlString)
        let baseURL: String = "https://api.openweathermap.org/data/2.5/onecall?lat=\(testLat)&lon=\(testLon)&appid=\(testActiveApiKey)&exclude=hourly,daily,alerts,minutely"
        
        XCTAssertEqual(urlComponents?.string, baseURL)

        
    }

    func testExample() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        // Any test you write for XCTest can be annotated as throws and async.
        // Mark your test throws to produce an unexpected failure when your test encounters an uncaught error.
        // Mark your test async to allow awaiting for asynchronous code to complete. Check the results with assertions afterwards.
    }

    func testSuccessfulResponse() {
        // Prepare mock response.
        
        let baseURL: String = "https://api.openweathermap.org/data/2.5/onecall?lat=\(testLat)&lon=\(testLon)&appid=\(testActiveApiKey)&exclude=hourly,daily,alerts,minutely"
        
        let data = MeteoResponse.mockedResponseJson.data(using: .utf8)
        
        MockURLProtocol.requestHandler = { request in
            guard let url = request.url, url.absoluteString == baseURL else {
                throw Environment.APIError.invalidURL
            }
            
            let response = HTTPURLResponse(url: URL(string: baseURL)!, statusCode: 200, httpVersion: nil, headerFields: nil)!
            return (response, data)
        }
        
        // Call API.
        
        weatherAPI.getCurrentWeather(with: testActiveApiKey, lat: testLat, lon: testLon) { result in
            switch result {
            case .success(let weather):
                XCTAssertEqual(weather.lon, Double(self.testLon), "Incorrect lon")
                XCTAssertEqual(weather.lat, Double(self.testLat), "Incorrect lat")
            case .failure(let error):
                XCTFail("Error was not expected: \(error)")
            }
            self.expectation.fulfill()
        }
        wait(for: [expectation], timeout: 1.0)
    }
    
    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
