// swift-interface-format-version: 1.0
// swift-compiler-version: Apple Swift version 5.5.2 (swiftlang-1300.0.47.5 clang-1300.0.29.30)
// swift-module-flags: -target arm64-apple-ios11.0 -enable-objc-interop -enable-library-evolution -swift-version 5 -enforce-exclusivity=checked -O -module-name WeatherFramwork
import Foundation
import Swift
@_exported import WeatherFramwork
import _Concurrency
public struct Alert : Swift.Codable {
  public func encode(to encoder: Swift.Encoder) throws
  public init(from decoder: Swift.Decoder) throws
}
public struct Rain : Swift.Codable {
  public func encode(to encoder: Swift.Encoder) throws
  public init(from decoder: Swift.Decoder) throws
}
public struct Daily : Swift.Codable {
  public let dt: Swift.Int?, sunrise: Swift.Int?, sunset: Swift.Int?, moonrise: Swift.Int?
  public let moonset: Swift.Int?
  public let moonPhase: Swift.Double?
  public let temp: WeatherFramwork.Temp?
  public let feelsLike: WeatherFramwork.FeelsLike?
  public let pressure: Swift.Int?, humidity: Swift.Int?
  public let dewPoint: Swift.Double?, windSpeed: Swift.Double?
  public let windDeg: Swift.Int?
  public let weather: [WeatherFramwork.Weather]?
  public let clouds: Swift.Int?
  public let pop: Swift.Double?, rain: Swift.Double?, uvi: Swift.Double?
  public func encode(to encoder: Swift.Encoder) throws
  public init(from decoder: Swift.Decoder) throws
}
public struct FeelsLike : Swift.Codable {
  public let day: Swift.Double?, night: Swift.Double?, eve: Swift.Double?, morn: Swift.Double?
  public func encode(to encoder: Swift.Encoder) throws
  public init(from decoder: Swift.Decoder) throws
}
public struct Temp : Swift.Codable {
  public let day: Swift.Double?, min: Swift.Double?, max: Swift.Double?, night: Swift.Double?
  public let eve: Swift.Double?, morn: Swift.Double?
  public func encode(to encoder: Swift.Encoder) throws
  public init(from decoder: Swift.Decoder) throws
}
public struct Minutely : Swift.Codable {
  public let dt: Swift.Int?
  public let precipitation: Swift.Double?
  public func encode(to encoder: Swift.Encoder) throws
  public init(from decoder: Swift.Decoder) throws
}
public struct Current : Swift.Codable {
  public let dt: Swift.Int?, sunrise: Swift.Int?, sunset: Swift.Int?
  public let temp: Swift.Double?, feelsLike: Swift.Double?
  public let pressure: Swift.Int?, humidity: Swift.Int?
  public let dewPoint: Swift.Double?, uvi: Swift.Double?
  public let clouds: Swift.Int?, visibility: Swift.Int?
  public let windSpeed: Swift.Double?
  public let windDeg: Swift.Int?
  public let weather: [WeatherFramwork.Weather]?
  public let rain: WeatherFramwork.Rain?
  public let windGust: Swift.Double?
  public let pop: Swift.Int?
  public func encode(to encoder: Swift.Encoder) throws
  public init(from decoder: Swift.Decoder) throws
}
public struct Environment {
  public enum APIError : Swift.Error {
    case decodingFailer
    case networkFailer
    case invalidURL
    public static func == (a: WeatherFramwork.Environment.APIError, b: WeatherFramwork.Environment.APIError) -> Swift.Bool
    public func hash(into hasher: inout Swift.Hasher)
    public var hashValue: Swift.Int {
      get
    }
  }
  public enum API {
  }
}
public class WeatherSDK {
  public init(apiKey: Swift.String)
  public func getCurrentWeather(from lat: Swift.String, lon: Swift.String, completion: @escaping (Swift.Result<WeatherFramwork.MeteoResponse, WeatherFramwork.Environment.APIError>) -> Swift.Void)
  @objc deinit
}
public struct Weather : Swift.Codable {
  public let id: Swift.Int?
  public let main: Swift.String?, weatherDescription: Swift.String?, icon: Swift.String?
  public func encode(to encoder: Swift.Encoder) throws
  public init(from decoder: Swift.Decoder) throws
}
public struct MeteoResponse : Swift.Codable {
  public let lat: Swift.Double?, lon: Swift.Double?
  public let timezone: Swift.String?
  public let timezoneOffset: Swift.Int?
  public let current: WeatherFramwork.Current?
  public let minutely: [WeatherFramwork.Minutely]?
  public let hourly: [WeatherFramwork.Current]?
  public let daily: [WeatherFramwork.Daily]?
  public let alerts: [WeatherFramwork.Alert]?
  public func encode(to encoder: Swift.Encoder) throws
  public init(from decoder: Swift.Decoder) throws
}
extension WeatherFramwork.Environment.APIError : Swift.Equatable {}
extension WeatherFramwork.Environment.APIError : Swift.Hashable {}
